/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// These are private/internal typedefs, as TCB, stack_t, etc...


#ifndef RTOS_PRIVATE_TYPES_H_
#define RTOS_PRIVATE_TYPES_H_


#include "../../../LIBS/STD_TYPES/LSTD_TYPES.h"
#include "../RTOS_public_types.h"

// stack type
typedef u32 RTOS_stack_t;

// task state
typedef enum Task_state
{
    RTOS_state_uninitialized, // the default state for any task, at initialization of RTOS

    RTOS_state_suspended, // fancy term for paused!

    RTOS_state_idle,    // on hold, but ready if nothing else is running
    RTOS_state_resumed, // running

    RTOS_state_critical_running, // a critical task that is currently running/resumed

    RTOS_state_delayed,               // waiting for a specific timeout
    RTOS_state_blocked_bin_count_sem, // tried to acquire an already acquired semaphore (bin or counting)

    // Error states
    RTOS_state_ERROR,                            // placeholder for a generic error, also allows this: if (state >= RTOS_state_ERROR)
    RTOS_state_ERROR_Stack_Overflow,             // stack overflow
    RTOS_state_ERROR_bin_sem_already_free,       // bin semaphore already free
    RTOS_state_ERROR_count_sem_max_free,         // count semaphore max free-s reached
    RTOS_state_ERROR_unentered_critical_section, // task tried to leave a critical section that was never entered
    RTOS_state_ERROR_multi_critical_sections,    // task tried to enter an already-entered critical section
    RTOS_state_ERROR_illegal_request,            // task requested an inexistent kernel service
} RTOS_state_t;

// action to take on error
typedef enum Error_action
{
    RTOS_action_delete,
    RTOS_action_reset,
    RTOS_action_continue,

    RTOS_action_NO_CB // placeholder, used as non effective value (no callback used)
} RTOS_action_t;

// TCB (Task Control Block)
/*
 NOTE: The order of the members is very important, the first one must be 'stack_ptr' because later we need to access it in asm
       hence, its address relative to 'current_task' must be definite.
       The second member must be the one that gets repeated frequently in the switching algorithm, because is asm (after compilation)
       the way this is accessed is by the Z, Y, or X pointers + displacement, i.e "STD Z+q, Rn", where 'q' has limited range, so
       again the distance of the second member relative to 'current_task' must be as close as possible.
       When this is not satisfied, the compiler generates 1 or 2 more asm instructions to adjust the distance, ADD or ADW,
       which increases the number of clocks and subsequently, the execution time.
       The rest of the members should follow the same rule, they must be layed out in order of frequency (rate of usage),
       to decrease the possibility of generating an overhead for distance adjustment.
*/
typedef struct TCB
{
    RTOS_SP_t stack_ptr;        // points at the current stack (wherever that is)
    RTOS_state_t state;         // task state {running, idle, etc...}
    u16 ticks;                  // ticks count, used when the task is delayed to determine the timeout
    RTOS_sem_handle_t sem_addr; // holds semaphore address, used when task is blocked to check whether semaphore is still acquired
    RTOS_state_t last_state;    // task previous state {running, idle, etc...}, used when resuming a paused task
    RTOS_SP_t stack_top;        // always points at the top of the stack, used when checking for stack overflow
    u16 stack_user_size;        // size of the user-space stack (bytes), used when resetting/returning
    RTOS_PC_t function_top;     // always points at the address of the first instruction, used when resetting/returning
} RTOS_task_t;


#endif /* RTOS_PRIVATE_TYPES_H_ */

