/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// These are the private objects used by RTOS.


#ifndef RTOS_PRIVATE_OBJECTS_H_
#define RTOS_PRIVATE_OBJECTS_H_


#include "../../../LIBS/STD_TYPES/LSTD_TYPES.h"
#include "RTOS_private_types.h"
#include "RTOS_private_defines.h"
#include "../RTOS_config.h"

/*
   Stack layout
 |--------------|
 |  Regs pushed | [0]
 |    by SW     | [1]
 |  <32 bytes>  |  .
 |--------------|  .
 |  Regs pushed |  .
 |    by HW     |  .
 |  <32 bytes>  |  .
 |--------------|  .
 |  User space  |  .
 |  <N bytes>   |  .
 |--------------|  .
 | reset() addr |  .
 |   <4 bytes>  | [4 + 32 + 36 + N - 1]
 |--------------|
*/

// internal stack (if required)
#if (RTOS_USE_EXTERNAL_STACK == 0)
 // stack length including: {regs pushed by SW/HW, user stack size, addr of 'reset()'}
static RTOS_stack_t stack[RTOS_USER_TASKS_LEN][RTOS_INTERNAL_STACK_LEN];
#endif // RTOS_USE_EXTERNAL_STACK

static volatile RTOS_stack_t stack_bg_task[RTOS_BACKGROUND_STACK_LEN] __attribute__((used)) = {0};

// task-related objects
static RTOS_task_t tasks[RTOS_TASKS_LEN];                               // tasks array
static RTOS_task_t* const background_task = &(tasks[RTOS_TASKS_LEN-1]); // always points at the last task
static RTOS_task_t* current_task = &(tasks[RTOS_TASKS_LEN-1]);          // points at the current task

// global ticks counter
/*
 HACK: on older versions of GCC, the variable symbol/signature was not generated
       because it's not changed anywhere as a normal C variable (asm only),
       even though it's mentioned in the context-switching function, it's only read (not changed)
       hence, the attribute 'used' is used!
*/
static volatile u16 RTOS_u16Ticks_ms __attribute__((used)) = 0xFFFF; // initialized to MAX so that the first RTOS interrupt would have tick count = 0

// critical section objects
static u8 isInsideCriticalSection = 0; // are we inside a critical section or not
static RTOS_task_t* critical_task;     // points at the task that's inside the critical section

/*
 This is a play-field, used when local variables need to be created,
 return addresses need to be pushed, or for any other purposes.
 This is used by internal and kernel functions that rely on handlers to do their job,
 since these handlers are normal-C code, they'll allocate local variables, push return addresses, etc...
 hence to avoid cluttering and wasting the stack of the current task, SP is directed to this field
 after saving the task's context, where RTOS is free to move around (allocate variables, etc...).
*/
/*
 HACK: on older versions of GCC, the variable symbol/signature was not generated
       because it's not referenced or changed anywhere as a normal C variable (asm only),
       hence, the attribute 'used' is used!
*/
static volatile RTOS_stack_t RTOS_playfield[RTOS_PLAYFIELD_LEN] __attribute__((used));

// Error-handling callbacks
#if (RTOS_USE_ERROR_CALLBACK != 0)
static RTOS_error_CB_t RTOS_CB_error = 0;          // callback when normal task does an illegal action (stack overflow, etc...)
static RTOS_error_CB_t RTOS_CB_critical_error = 0; // callback when a critical task does a blocking action (delay, block sem, etc...)
#endif // RTOS_USE_ERROR_CALLBACK


#endif /* RTOS_PRIVATE_OBJECTS_H_ */

