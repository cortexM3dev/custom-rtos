/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// These are private/internal defines.


#ifndef RTOS_PRIVATE_DEFINES_H_
#define RTOS_PRIVATE_DEFINES_H_


#include "../RTOS_public_types.h"
#include "../RTOS_config.h"

// 8 by HW {r0-r3, r12, LR, PC, xPSR} + 8 by SW {r4-r11}
#define RTOS_HW_REGS_LEN 8
#define RTOS_SW_REGS_LEN 8
#define RTOS_REGS_LEN (RTOS_HW_REGS_LEN + RTOS_SW_REGS_LEN)
// size in bytes
#define RTOS_REGS_SIZE (RTOS_REGS_LEN*sizeof(u32))

// +sizeof(RTOS_PC_t) for address of 'reset()'
#define RTOS_INTERNAL_STACK_SIZE (RTOS_REGS_SIZE + RTOS_USER_STACK_SIZE + sizeof(RTOS_PC_t))
// length
#define RTOS_INTERNAL_STACK_LEN (RTOS_INTERNAL_STACK_SIZE/sizeof(RTOS_stack_t))

// size of stack used by background task (in bytes)
#define RTOS_BACKGROUND_STACK_SIZE 200
// length
#define RTOS_BACKGROUND_STACK_LEN (RTOS_BACKGROUND_STACK_SIZE/sizeof(RTOS_stack_t))

// +1 for background task
#define RTOS_TASKS_LEN (RTOS_USER_TASKS_LEN + 1)

// size of the internal Playfield stack (in bytes)
#if (RTOS_USE_ERROR_CALLBACK == 0)
#define RTOS_PLAYFIELD_SIZE (156)
#else // give more space for callbacks
#define RTOS_PLAYFIELD_SIZE (156*2)
#endif // RTOS_USE_ERROR_CALLBACK
// length
#define RTOS_PLAYFIELD_LEN (RTOS_PLAYFIELD_SIZE/sizeof(RTOS_stack_t))


#endif /* RTOS_PRIVATE_DEFINES_H_ */

