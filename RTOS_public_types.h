/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


// These are the public typedefs.


#ifndef RTOS_PUBLIC_TYPES_H_
#define RTOS_PUBLIC_TYPES_H_


#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"

// task state
typedef enum
{
    RTOS_task_state_uninitialized, // the default state for any task, at initialization of RTOS

    RTOS_task_state_suspended, // task is paused (must be resumed by another task)

    RTOS_task_state_idle,    // task is on hold (waiting for its turn)
    RTOS_task_state_resumed, // task is currently running

    RTOS_task_state_critical_running, // task is inside a critical section and is currently running/resumed

    RTOS_task_state_delayed,               // waiting for a specific timeout
    RTOS_task_state_blocked_bin_count_sem, // tried to acquire an already acquired semaphore (bin or counting)

    // Error states
    RTOS_task_state_ERROR,                            // placeholder for a generic error
    RTOS_task_state_ERROR_Stack_Overflow,             // stack overflow
    RTOS_task_state_ERROR_Bin_Semaphore_Already_Free, // task tried to free an already free bin semaphore
    RTOS_task_state_ERROR_unentered_critical_section, // task tried to leave a critical section that was never entered
    RTOS_task_state_ERROR_multi_critical_sections,    // task tried to enter an already-entered critical section
} RTOS_task_state_t;

// action to take on error
typedef enum
{
    RTOS_error_action_delete,
    RTOS_error_action_reset,
    RTOS_error_action_continue,
} RTOS_error_action_t;

// stack-pointer type
typedef u32* RTOS_SP_t;

// function type
typedef void (*RTOS_PC_t)(void);

// error-callback type
typedef RTOS_error_action_t (*RTOS_error_CB_t)(const u8 u8IndexCpy, const RTOS_task_state_t enumErrorCpy);

// semaphore types
typedef volatile u16 RTOS_semaphore_t;
typedef RTOS_semaphore_t* RTOS_sem_handle_t;


#endif /* RTOS_PUBLIC_TYPES_H_ */

