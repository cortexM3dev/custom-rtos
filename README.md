## Custom RTOS

A Real Time OS with modified Round Robin algorithm, that prefers events based on tasks priorities.

# Features:

- Tasks creation and deletion

- Delaying tasks

- Support for Binary/Counting Semaphonres

- Critical Section suppport

- Stack Overflow check

- APIs to query task's states, and interal tick count

- Ability to use externally-provided stack per task, or use internally-provided stacks

- Software emulated interrupts, use for debugging purposes when a task does an illegal action

- Safe stacks, RTOS separates the stack used in user space (PSP) and kernel memory space (MSP)
