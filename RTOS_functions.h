/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef RTOS_FUNCTIONS_H_
#define RTOS_FUNCTIONS_H_


#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"
#include "RTOS_public_types.h"
#include "RTOS_config.h"

// --- convenience defines --- //
#define RTOS_NO_DELAY 0
#define RTOS_HIGHEST_PRIORITY 0

#define RTOS_PAUSED 1
#define RTOS_RESUMED 0

#define RTOS_SEM_ACQUIRED 0
#define RTOS_SEM_FREE     1
// --------------------------- //

// --- task declaration and definition --- //
#define RTOS_DECL_TASK(func) void func(void) __attribute__((used, noinline))
#define RTOS_TASK(func) void func(void)
// -------------------------------------- //

// --- external stack declaration --- //
#if (RTOS_USE_EXTERNAL_STACK != 0)
    #define RTOS_CREATE_STACK(identifier, user_len_bytes) u32 identifier[8 + 8 + user_len_bytes/sizeof(u32) + 1] __attribute__((used, externally_visible))
#endif // RTOS_USE_EXTERNAL_STACK
// ---------------------------------- //

void RTOS_init(u32 AHB_clk);
void RTOS_start(void);

#if (RTOS_USE_EXTERNAL_STACK == 0)
void RTOS_registerTask(const RTOS_PC_t funcPtrFunctionCpy, const u16 u16Skip_msCpy, const u8 u8isPaused, const u8 u8IndexCpy);
#else
void RTOS_registerTask(const RTOS_PC_t funcPtrFunctionCpy, const u16 u16Skip_msCpy, const u8 u8isPaused, const RTOS_SP_t stackPtrStkCpy, const u16 u16StkLenCpy, const u8 u8IndexCpy);
#endif // RTOS_USE_EXTERNAL_STACK

void RTOS_deregisterTask(const u8 u8IndexCpy);

// skip the next 'N' slices, then timeout and start at slice 'N+1'
void RTOS_skip(u16 u16Skip_msCpy);
#define RTOS_sleep(n) RTOS_skip(n - 1)

void RTOS_acquireBinSem(RTOS_sem_handle_t semHandleCpy);
#define RTOS_lockBinSem(sem) RTOS_acquireBinSem(&sem)
void RTOS_releaseBinSem(RTOS_sem_handle_t semHandleCpy);
#define RTOS_freeBinSem(sem) RTOS_releaseBinSem(&sem)

void RTOS_acquireCountingSem(RTOS_sem_handle_t semHandleCpy);
#define RTOS_getFromCountingSem(sem) RTOS_acquireCountingSem(&sem)
void RTOS_releaseCountingSem(RTOS_sem_handle_t semHandleCpy);
#define RTOS_giveToCountingSem(sem) RTOS_releaseCountingSem(&sem)
void RTOS_releaseCountingSemSafe(RTOS_sem_handle_t semHandleCpy, u16 maxCount);

void RTOS_enterCriticalSection(void);
void RTOS_leaveCriticalSection(void);

void RTOS_pauseCurrentTask(void);
#define RTOS_pause() RTOS_pauseCurrentTask()
void RTOS_pauseTask(const u8 u8IndexCpy);
void RTOS_resumeTask(const u8 u8IndexCpy);

void RTOS_resetCurrentTask(void);
#define RTOS_return() RTOS_resetCurrentTask()
void RTOS_resetTask(const u8 u8IndexCpy);

void RTOS_giveUp(void);

u16 RTOS_getTicksCount(void);

RTOS_task_state_t RTOS_getCurrentTaskState(void);
RTOS_task_state_t RTOS_getTaskState(const u8 u8IndexCpy);

#if (RTOS_USE_ERROR_CALLBACK != 0)
void RTOS_registerErrorCB(const RTOS_error_CB_t funcPtrErrorCBCpy);
void RTOS_deregisterErrorCB(void);
void RTOS_registerCriticalErrorCB(const RTOS_error_CB_t funcPtrErrorCBCpy);
void RTOS_deregisterCriticalErrorCB(void);
#endif // RTOS_USE_ERROR_CALLBACK


#endif /* RTOS_FUNCTIONS_H_ */

