/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// These are compiler-specific defines and/or implementations.


#ifndef RTOS_COMPILER_SPECIFIC_H_
#define RTOS_COMPILER_SPECIFIC_H_


/*
 From GCC official docs, AVR function attributes:
 For attributes 'OS_main' and 'OS_task', the differences to the 'naked' function attribute are:
 1) naked functions do not have a return instruction whereas OS_main and OS_task functions have a RET or RETI return instruction.
 2) naked functions do not set up a frame for local variables or a frame pointer whereas OS_main and OS_task do this as needed.

 The 'OS_main' attribute can be used when there <is guarantee> that interrupts are disabled at the time when the function is entered.
 This saves resources when the stack pointer has to be changed to set up a frame for local variables.

 The 'OS_task' attribute can be used when there <is no guarantee> that interrupts are disabled at that time when the function is entered,
 like for e.g. task functions in a multi-threading operating system.
 In that case, changing the stack pointer register is guarded by save/clear/restore of the global interrupt enable flag.


 attribute 'noinline' prevents the optimizer from inlining the function under any circumstances, even if it wants to
 attribute 'used' prevents the optimizer from removing (optimizing away) the function if it's unused (or for any other circumstances)
 attribute 'externally_visible' exposes the function to other compilation units (opposite of 'static')


 NOTE: GCC docs explicitly state that normal-C and extended-asm are not supported in 'naked' functions,
       hence, kernel functions that require many/heavy normal-C syntax are NOT marked as 'naked',
       they're also marked as 'noinline' so that the compiler is forced to generate proper pro/epi-logue and body.
*/

// this stringifies tokens, if it's a macro, then it's expanded first
#define AS_STR(x) #x
#define STR(x) AS_STR(x)

// 'RTOS_KERNEL_xxx' are the internal/static functions
#define RTOS_KERNEL_INLINE_DECL(func, ...) static inline void func(__VA_ARGS__) __attribute__((always_inline, used))
#define RTOS_KERNEL_INLINE(func, ...)      static inline void func(__VA_ARGS__)

#define RTOS_KERNEL_NOINLINE_DECL(func, ...) static void func(__VA_ARGS__) __attribute__((noinline, used, pcs("aapcs")))
#define RTOS_KERNEL_NOINLINE(func, ...)      static void func(__VA_ARGS__)

#define RTOS_KERNEL_NOINLINE_NAKED_DECL(func, ...) static void func(__VA_ARGS__) __attribute__((noinline, naked, used, pcs("aapcs")))
#define RTOS_KERNEL_NOINLINE_NAKED(func, ...)      static void func(__VA_ARGS__)


// 'RTOS_API_xxx' are the external functions used in the APP layer
/*
 BUGFIX: 'RTOS_API' functions when defined with 'OS_main' or 'OS_task' attribute will for some reason corrupt variables
         defined before calling them, hence they're removed from all 'RTOS_XXX' macros
*/
#define RTOS_API(retType, func, ...) retType func(__VA_ARGS__) __attribute__((noinline, externally_visible, used, pcs("aapcs"))); \
                                     retType func(__VA_ARGS__)

#define RTOS_API_NAKED(retType, func, ...) retType func(__VA_ARGS__) __attribute__((noinline, naked, externally_visible, used, pcs("aapcs"))); \
                                           retType func(__VA_ARGS__)


#endif /* RTOS_COMPILER_SPECIFIC_H_ */

