/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// These are private microcontroller-based functions/macros to do low-level operations in asm.


#ifndef RTOS_MCU_SPECIFIC_H_
#define RTOS_MCU_SPECIFIC_H_


// macros to push 1 GP register
#define RTOS_PUSH_REG(n) asm volatile("push {" #n "} \t\n")

// macros to pop 1 GP register
#define RTOS_POP_REG(n) asm volatile("pop {" #n "} \t\n")

// macros to disable/enable global interrupt
#define RTOS_DISABLE_GINT() \
    /* Disable interrupts and configurable fault handlers (set PRIMASK) */ \
    asm volatile("CPSID i \t\n"); \
    /* Disable interrupts and all fault handlers (set FAULTMASK) */ \
    asm volatile("CPSID f \t\n")

#define RTOS_ENABLE_GINT() \
    /* Enable interrupts and fault handlers (clear FAULTMASK) */ \
    asm volatile("CPSIE f \t\n"); \
    /* Enable interrupts and configurable fault handlers (clear PRIMASK) */ \
    asm volatile("CPSIE i \t\n")

// macros to return from ISR, or a function if it's naked
#define RTOS_RETI asm volatile("bx lr \t\n")
#define RTOS_RET  RTOS_POP_REG(pc)

// macro to push whole task context (GP regs + SREG)
#define RTOS_PUSH_MACHINE_STATE() \
    asm volatile(         \
                 "mrs r0, psp \t\n" /* r0 = PSP */ \
                 "stmdb r0!, {r4-r11} \t\n" /* *(--PSP) = {r11, r10, ..., r4} */ \
                )

// macro to save current SP in 'current_task->stack_ptr'
#define RTOS_SAVE_SP()                                                                     \
    asm volatile(                                                                          \
                 /* here 'current_task' is replaced by its address, i.e '&current_task' */ \
                 "ldr r1, =current_task \t\n"                     /* r1 = &current_task */ \
                 "ldr r1, [r1] \t\n"                         /* r1 = *r1 = current_task */ \
                 /* store PSP in *current_task->stack_ptr */                               \
                 "str r0, [r1] \t\n"                             /* *current_task = PSP */ \
                )

// macro to point SP at the Playfield
#define RTOS_POINT_SP_TO_PLAYFIELD()                                                  \
    asm volatile(                                                                     \
                 "ldr r0, =RTOS_playfield \t\n"            /* r0 = &RTOS_playfield */ \
                 "add r0, " STR(RTOS_PLAYFIELD_SIZE-4) " \t\n"            /* r0 += len-1 */ \
                 "msr msp, r0 \t\n"                                    /* MSP = r0 */ \
                )

// macro to save: {PC (implicit by H/W), GP regs, SREG, SP}, then, point at Playfield
#define RTOS_SAVE_CURRENT_CONTEXT()                                        \
    /* push machine state (GP regs + SREG), PC is already pushed by H/W */ \
    RTOS_PUSH_MACHINE_STATE();                                             \
    /* save stack pointer of current task */                               \
    RTOS_SAVE_SP();                                                        \
    /* point SP to the Playfield so we can move around safely */           \
    RTOS_POINT_SP_TO_PLAYFIELD()

// macro to increment ticks counter
#define RTOS_INC_TICKS_COUNTER() RTOS_u16Ticks_ms++


// macro to restore current SP from 'current_task->stack_ptr'
#define RTOS_RESTORE_SP()                                                                  \
    asm volatile(                                                                          \
                 /* here 'current_task' is replaced by its address, i.e '&current_task' */ \
                 "ldr r0, =current_task \t\n"                     /* r0 = &current_task */ \
                 "ldr r0, [r0] \t\n"                               /* r0 = current_task */ \
                 "ldr r0, [r0] \t\n"                              /* r0 = *current_task */ \
                )

// macro to pop whole task context
#define RTOS_POP_MACHINE_STATE() \
    asm volatile(         \
                 "ldmia r0!, {r4-r11} \t\n" /* *(PSP++) = {r4, r5, ..., r11} */ \
                 /* store PSP in *current_task->stack_ptr */ \
                 "msr psp, r0 \t\n" /* PSP = r0 */ \
                )

// macro to restore PC, GP regs, SREG, SP
#define RTOS_RESTORE_NEW_CONTEXT()       \
    /* get stack pointer of next task */ \
    RTOS_RESTORE_SP();                   \
    /* restore registers */              \
    RTOS_POP_MACHINE_STATE()

// macro to point PSP at background stack
#define RTOS_POINT_PSP_TO_BG_STACK() \
    asm volatile( \
                 "ldr r0, =stack_bg_task \t\n" /* r0 = &stack_bg_task */ \
                 "add r0, #" STR(RTOS_BACKGROUND_STACK_SIZE-4) " \t\n" /* r0 += len-1 */ \
                 "msr psp, r0 \t\n" /* PSP = r0 */ \
                )

// macro to change mode to: Thread, using PSP
#define RTOS_SWITCH_MODE_TO_UNPRIVILEGED() \
    asm volatile( \
                 "mrs r0, control \t\n" /* r0 = CONTROL */ \
                 "orr r0, #0x00000003 \t\n" /* use PSP and switch to unprivileged mode */ \
                 "msr control, r0 \t\n" \
                 "isb \t\n" \
                )

// macro to pend a context switch
#define RTOS_PEND_CONTEXT_SWITCH() PSCB_setPendSVpending(1)

// macro to request a SVC
#define RTOS_REQUEST_SVC(n) asm volatile("svc " #n " \t\n")

// macro to request deregister/delete certain task
#define RTOS_SVC_DELETE_CERTAIN_TASK()      RTOS_REQUEST_SVC(0)

// macro to request delay current task
#define RTOS_SVC_DELAY_CURRENT_TASK()       RTOS_REQUEST_SVC(1)

// macro to request acquire a bin semaphore
#define RTOS_SVC_ACQ_BIN_SEM()              RTOS_REQUEST_SVC(2)

// macro to request release a bin semaphore
#define RTOS_SVC_REL_BIN_SEM()              RTOS_REQUEST_SVC(3)

// macro to request acquire a counting semaphore
#define RTOS_SVC_ACQ_COUNT_SEM()            RTOS_REQUEST_SVC(4)

// macro to request release a counting semaphore
#define RTOS_SVC_REL_COUNT_SEM()            RTOS_REQUEST_SVC(5)

// macro to request release a safe counting semaphore
#define RTOS_SVC_REL_SAFE_COUNT_SEM()       RTOS_REQUEST_SVC(6)

// macro to request enter critical section
#define RTOS_SVC_ENTER_CRITICAL_SECTION()   RTOS_REQUEST_SVC(7)

// macro to request leave critical section
#define RTOS_SVC_LEAVE_CRITICAL_SECTION()   RTOS_REQUEST_SVC(8)

// macro to request pause current task
#define RTOS_SVC_PAUSE_CURRENT_TASK()       RTOS_REQUEST_SVC(9)

// macro to request pause certain task
#define RTOS_SVC_PAUSE_CERTAIN_TASK()       RTOS_REQUEST_SVC(10)

// macro to request reset current task
#define RTOS_SVC_RESET_CURRENT_TASK()       RTOS_REQUEST_SVC(11)

// macro to request reset certain task
#define RTOS_SVC_RESET_CERTAIN_TASK()       RTOS_REQUEST_SVC(12)

// macro to request context switch
#define RTOS_SVC_CONTEXT_SWITCH()           RTOS_REQUEST_SVC(13)


#endif /* RTOS_MCU_SPECIFIC_H_ */

