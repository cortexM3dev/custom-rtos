/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// LIBS
#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"
// PAL
#include "../../PAL/SYSTICK/PSYSTICK_config.h"
#include "../../PAL/SYSTICK/PSYSTICK_interface.h"
#include "../../PAL/SCB/PSCB_interface.h"
// own files
#include "RTOS_config.h"
#include "RTOS_public_types.h"
#include "RTOS_functions.h"
#include "private_files/RTOS_private_defines.h"
#include "private_files/RTOS_private_types.h"
#include "private_files/RTOS_private_objects.h"
#include "impl_specific_files/RTOS_compiler_specific.h"
#include "impl_specific_files/RTOS_mcu_specific.h"

#ifdef RTOS_COOKIE_


// sanity checks
#if (RTOS_COOKIE_ < 0) || (RTOS_COOKIE_ > 2)
    #error "RTOS ERROR: RTOS_COOKIE_ is out of range, accepted values are: 0, 1, or 2."
#endif

#if !defined(RTOS_USE_EXTERNAL_STACK)
    #error "RTOS ERROR: RTOS_USE_EXTERNAL_STACK is undefined."
#endif // RTOS_USE_EXTERNAL_STACK

#if !defined(RTOS_USER_STACK_SIZE)
    #error "RTOS ERROR: RTOS_USER_STACK_SIZE is undefined."
#endif // RTOS_USER_STACK_SIZE

#if !defined(RTOS_USER_TASKS_LEN)
    #error "RTOS ERROR: RTOS_USER_TASKS_LEN is undefined."
#endif // RTOS_USER_TASKS_LEN

#if !defined(RTOS_OVERFLOW_CHECK)
    #error "RTOS ERROR: RTOS_OVERFLOW_CHECK is undefined."
#endif // RTOS_OVERFLOW_CHECK

#if !defined(RTOS_USE_ERROR_CALLBACK)
    #error "RTOS ERROR: RTOS_USE_ERROR_CALLBACK is undefined."
#endif // RTOS_USE_ERROR_CALLBACK


// --- internal --- //
// *** kernel functions prototypes *** /
RTOS_KERNEL_INLINE_DECL(RTOS_incCurrentTaskPtr, void);

#if (RTOS_OVERFLOW_CHECK != 0)
RTOS_KERNEL_NOINLINE_DECL(RTOS_handleOverflow, void);
#endif // RTOS_OVERFLOW_CHECK

#if (RTOS_USE_ERROR_CALLBACK != 0)
RTOS_KERNEL_NOINLINE_DECL(RTOS_handleErrorCB, RTOS_state_t enumErrorCpy);
#endif // RTOS_USE_ERROR_CALLBACK

RTOS_KERNEL_NOINLINE_DECL(RTOS_getNextTask, void);
RTOS_KERNEL_NOINLINE_DECL(RTOS_sysSrvDispatcher, const u8 request_num, u32* args);
RTOS_KERNEL_NOINLINE_DECL(RTOS_implHandleDelete, RTOS_task_t* taskPtrTaskCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_implHandleReset, RTOS_task_t* taskPtrTaskCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_implHandlePause, RTOS_task_t* taskPtrTaskCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_sysSrvHnd_delete, const u8 u8IndexCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_sysSrvHnd_acqBinSem, RTOS_sem_handle_t semHandleCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_sysSrvHnd_relBinSem, RTOS_sem_handle_t semHandleCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_sysSrvHnd_acqCountSem, RTOS_sem_handle_t semHandleCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_sysSrvHnd_relCountSem, RTOS_sem_handle_t semHandleCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_sysSrvHnd_relCountSemSafe, RTOS_sem_handle_t semHandleCpy, u16 maxCount);
RTOS_KERNEL_NOINLINE_DECL(RTOS_sysSrvHnd_enterCriticalSection, void);
RTOS_KERNEL_NOINLINE_DECL(RTOS_sysSrvHnd_leaveCriticalSection, void);
RTOS_KERNEL_NOINLINE_DECL(RTOS_sysSrvHnd_pause, const u8 u8IndexCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_sysSrvHnd_skip, const u16 u16Skip_msCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_sysSrvHnd_reset, const u8 u8IndexCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_sysSrvHnd_illegal, void);
// ************ //

// *** INLINE functions: these are spreaded in place where they're called *** //
// circular increment of 'current_taks' pointer
RTOS_KERNEL_INLINE(RTOS_incCurrentTaskPtr, void)
{
    // point at next task
    current_task += 1;
    // circular increment
    if (current_task == (tasks + RTOS_TASKS_LEN))
    {
        current_task = &(tasks[0]);
    }
}
// ************ //

// *** NOINLINE functions: these are handler functions, usually called by services dispatcher *** //
// overflow check
#if (RTOS_OVERFLOW_CHECK != 0)
RTOS_KERNEL_NOINLINE(RTOS_handleOverflow, void)
{// TODO: fix this maybe !!
    // if (stack overflow happened)
    // if (current stack addr < max allowed top addr) -> stack grows from bot to top
    if ( (current_task->stack_ptr) < (current_task->stack_top) )
    {
        current_task->state = RTOS_state_ERROR_Stack_Overflow; // set state as 'Stack Overflow'

#if (RTOS_USE_ERROR_CALLBACK != 0)
        RTOS_handleErrorCB(RTOS_state_ERROR_Stack_Overflow);
#endif // RTOS_USE_ERROR_CALLBACK
    }
}
#endif // RTOS_OVERFLOW_CHECK

// error-handling callback system
#if (RTOS_USE_ERROR_CALLBACK != 0)
RTOS_KERNEL_NOINLINE(RTOS_handleErrorCB, RTOS_state_t enumErrorCpy)
{
    register RTOS_action_t error_action = RTOS_action_NO_CB;

    if (isInsideCriticalSection)
    {
        isInsideCriticalSection = 0;
        
        if (RTOS_CB_critical_error)
        {
            error_action = RTOS_CB_critical_error((u8)(current_task - tasks), enumErrorCpy);
        }
    }
    else
    {
        if (RTOS_CB_error)
        {
            error_action = RTOS_CB_error((u8)(current_task - tasks), enumErrorCpy);
        }
    }
    
    if (error_action != RTOS_action_NO_CB)
    {
        switch (error_action)
        {
            case RTOS_action_delete:
                RTOS_implHandleDelete(current_task);
            break;

            case RTOS_action_reset:
                RTOS_implHandleReset(current_task);
            break;

            case RTOS_action_continue:
            default:
            break;
        }
    }
}
#endif // RTOS_USE_ERROR_CALLBACK

// switching algorithm
RTOS_KERNEL_NOINLINE(RTOS_getNextTask, void)
{
    // --- update current task state
    // only 'resumed' tasks are set to 'idle',
    // needed because 'delayed', 'blocked', etc... tasks must not be set to 'idle' blindly
    // also other states indicate either error-ed or paused state
    if (current_task->state == RTOS_state_resumed)
    {
        current_task->state = RTOS_state_idle;
    }
    // ------ //

    // --- update all tasks states and detect higher priority or abandoned ones
    // those are place-holders to indicate the result of the next loop
    static RTOS_task_t* abandoned_task = 0; // points at a previously abandoned task (if any)
    static u8 isAbandonedFound = 0;

    // if (we're not inside a critical section), then point at abandoned task (if any) or get next available task
    if (!isInsideCriticalSection)
    {
        // if (we previously abandoned a task), then point at it
        // all calculations (previous, etc...) should be relative to it's position
        if (isAbandonedFound)
        {
            current_task = abandoned_task;
            isAbandonedFound = 0;
        }
        else // if (no abandoned task found), then get the next available task
        {
            // get the next available task ('idle' or 'resumed')
            do
            {
                RTOS_incCurrentTaskPtr();
            }
            while ( (current_task->state != RTOS_state_idle) && (current_task->state != RTOS_state_resumed) );

            // declare current task as 'resumed' (in case it was 'idle')
            current_task->state = RTOS_state_resumed;
        }
    }

    RTOS_task_t* previous_task = 0;  // points at previous task that wants to run (if any)
    u8 isPreviousFound = 0; // indicates whether 'previous_task' was already set or not
                            // needed because we want to catch the very-first previous task,
                            // hence, it prevents multiple assignments

    // cache the value of the volatile ticks counter since it won't change during the below loop
    register u16 RTOS_u16Ticks_ms_cached = (u16)RTOS_u16Ticks_ms;

    // adjust states of all tasks,
    // and detect the first previous task that wants to run (if any)
    for (register RTOS_task_t* this_task = &(tasks[0]); this_task < (tasks + RTOS_TASKS_LEN); this_task += 1)
    {
        // take an action based on each state
        switch (this_task->state)
        {
            case RTOS_state_delayed: // if this task is delayed (triggered by a timeout)
                // this is a circular counter, i.e. 0 == (0xFFFF + 1) -> (MAX + 1)
                // ex: if current ticks = 0xFFFF, and required delay = 3 ticks
                //     therefore task->ticks = 0xFFFF + 3 = 2
                //     and when subtracting:
                //     1) 0 - 2 = (0xFFFF + 1) - 2 = 0xFFFF - 1 = 0xFFFE (not zero)
                //     2) 1 - 2 = (0xFFFF + 2) - 2 = 0xFFFF + 0 = 0xFFFF (not zero)
                //     3) 2 - 2 = (0xFFFF + 3) - 2 = 0xFFFF + 1 = 0      (zero)
                // but max delays is 65535

                // the XOR value will be 0 when: 'RTOS_u16Ticks_ms' = 'this_task->ticks'
                if ( !(RTOS_u16Ticks_ms_cached ^ this_task->ticks) ) // ticks are finished
                {
                    this_task->state = RTOS_state_resumed;
                }
            break;

            case RTOS_state_suspended: // if this was a delayed function, but currently 'suspended'
                if (this_task->last_state == RTOS_state_delayed)
                {
                    // the XOR value will be 0 when: 'RTOS_u16Ticks_ms' = 'this_task->ticks'
                    if ( !(RTOS_u16Ticks_ms_cached ^ this_task->ticks) ) // ticks are finished
                    {
                        this_task->last_state = RTOS_state_resumed;
                    }
                }
            break;

            case RTOS_state_blocked_bin_count_sem: // if this task is blocked (waiting for a semaphore)
                if ( *(this_task->sem_addr) ) // if semaphore has counts (isn't completely acquired anymore)
                {
                    this_task->state = RTOS_state_resumed;

                    // acquire a count from this semaphore, because this task will use it
                    *(this_task->sem_addr) -= 1;
                }
            break;

            default: // ignore all other states (suppresses compiler warning)
            break;
        }

        // from this point, the task state is updated, next if it became 'resumed'
        // then detect if it has higher priority

        // detect first higher priority task that wants to run (if any)
        if ( (this_task->state == RTOS_state_resumed) )
        {
            if (!isPreviousFound)
            {            
                if (this_task < current_task)
                {
                    previous_task = this_task;
                    isPreviousFound = 1;
                }
            }
        }
    }
    // ------ //

    // --- point at the task whose turn is now
    // if we're inside critical section, then point at the critical task
    // else if there's a higher priority task wants to run, then point at it,
    // else, do nothing, which means run the currently selected task
    if (isInsideCriticalSection)
    {
        current_task = critical_task;

        // always set the task state as 'critical running', because during the critical section the task
        // might do illegal actions that result in a state != 'critical running' (delayed, blocked sem, etc...),
        // hence, the state must be re-set back to 'critical running' whenever a context switch happens.
        // this ensures correct task state, but obviously if an illegal action happened during a critical section,
        // it will be missed.

        // TODO: from the previous description, check first to see if the state stayed 'critical running',
        //       if not, then maybe call a handler/callback
        current_task->state = RTOS_state_critical_running;
    }
    else if (isPreviousFound)
    {
        // declare the current/available task as abandoned, for later round
        abandoned_task = current_task;
        isAbandonedFound = 1;

        // point at the higher priority task
        current_task = previous_task;
    }
    // ------ //
}

// system services dispatcher, calls required service handler(s)
RTOS_KERNEL_NOINLINE(RTOS_sysSrvDispatcher, const u8 request_num, u32* args)
{
    switch (request_num)
    {
        case 0: // delete certain task
            RTOS_sysSrvHnd_delete((u8)(args[0]));
        break;

        case 1: // delay current task
            RTOS_sysSrvHnd_skip((u16)(args[0]));
        break;

        case 2: // acquire a bin semaphore
            RTOS_sysSrvHnd_acqBinSem((RTOS_sem_handle_t)(args[0]));
        break;

        case 3: // release a bin semaphore
            RTOS_sysSrvHnd_relBinSem((RTOS_sem_handle_t)(args[0]));
        break;

        case 4: // acquire a counting semaphore
            RTOS_sysSrvHnd_acqCountSem((RTOS_sem_handle_t)(args[0]));
        break;

        case 5: // release a counting semaphore
             RTOS_sysSrvHnd_relCountSem((RTOS_sem_handle_t)(args[0]));
        break;

        case 6: // release a counting semaphore (safe)
             RTOS_sysSrvHnd_relCountSemSafe((RTOS_sem_handle_t)(args[0]), (u16)(args[1]));
        break;

        case 7: // enter critical section
            RTOS_sysSrvHnd_enterCriticalSection();
        break;

        case 8: // leave critical section
            RTOS_sysSrvHnd_leaveCriticalSection();
        break;

        case 9: // pause current task
            RTOS_sysSrvHnd_pause((u8)(current_task - tasks));
        break;

        case 10: // pause certain task
            RTOS_sysSrvHnd_pause((u8)(args[0]));
        break;

        case 11: // reset current task
            RTOS_sysSrvHnd_reset((u8)(current_task - tasks));
        break;

        case 12: // reset certain task
            RTOS_sysSrvHnd_reset((u8)(args[0]));
        break;

        case 13: // switch context
            RTOS_getNextTask();
        break;

        default:
            RTOS_sysSrvHnd_illegal();
        break;
    }
}

// implementation for 'RTOS_handleDelete(...)', also used by error callbacks when deleting
RTOS_KERNEL_NOINLINE(RTOS_implHandleDelete, RTOS_task_t* taskPtrTaskCpy)
{
    // set task state to 'uninitialized'
    taskPtrTaskCpy->state = RTOS_state_uninitialized;
}

// implementation for 'RTOS_handlePause()'
RTOS_KERNEL_NOINLINE(RTOS_implHandlePause, RTOS_task_t* taskPtrTaskCpy)
{
    // BUGFIX: pausing an already paused task will lock it forever!
    //         because the second pause will make 'last_state' = 'suspended'
    // ex: at start     -> last_state = idle, state = idle
    //     first cycle  -> state = resumed
    //     first pause  -> last_state = resumed, state = suspended
    //     second pause -> last_state = suspended, state = suspended (wrong)
    if ( (taskPtrTaskCpy->state == RTOS_state_uninitialized)
         ||(taskPtrTaskCpy->state == RTOS_state_suspended)
         || (taskPtrTaskCpy->state >= RTOS_state_ERROR) )
    {
        return;
    }

    // save current state and change it to 'suspended'
    // TODO: is this true ? this is a "blind" change of state, maybe more logic should be added
    taskPtrTaskCpy->last_state = taskPtrTaskCpy->state;
    taskPtrTaskCpy->state = RTOS_state_suspended;
}

// implementation for 'RTOS_handleReset(...)', also used by error callbacks when resetting
RTOS_KERNEL_NOINLINE(RTOS_implHandleReset, RTOS_task_t* taskPtrTaskCpy)
{
#if (RTOS_USE_EXTERNAL_STACK == 0)
    register const u16 stk_len = RTOS_INTERNAL_STACK_LEN;
#else // if using external stack
    register const u16 stk_len = RTOS_REGS_LEN + (taskPtrTaskCpy->stack_user_size)/sizeof(RTOS_stack_t) + 1;
#endif // RTOS_USE_EXTERNAL_STACK

    // cache task's stack
    register const RTOS_SP_t stk = taskPtrTaskCpy->stack_top;

    // clear stack (implicitly GP registers too), but don't clear 'reset()' addr, we'll add it later
    for (register RTOS_SP_t s = stk; s < (stk + stk_len - 1); s += 1)
    {
        *s = 0;
    }

    // initially no ticks
    taskPtrTaskCpy->ticks = 0;

    // initially no semaphores associated
    taskPtrTaskCpy->sem_addr = 0;

    // store the return address (address of first instruction)
    *(RTOS_PC_t*)(stk + stk_len - 1 - 2) = taskPtrTaskCpy->function_top;

    // store address of 'RTOS_resetCurrentTask' as last stack element to allow normal 'return' by compiler
    *(RTOS_PC_t*)(stk + stk_len - 1) = (RTOS_PC_t)RTOS_resetCurrentTask;

    // set Thumb-state bit in xPSR to 1, otherwise it causes HardFault exception
    *(RTOS_stack_t*)(stk + stk_len - 1 - 1) = 0x01000000;

    // point before PC and GP regs, so when it's pop-ed, stack pointer points at the beginning
    // of the stack (pop-ing increases pointer)
    taskPtrTaskCpy->stack_ptr = (RTOS_SP_t)(stk + stk_len - 1 - RTOS_REGS_LEN);

    // set initial and previous states to 'idle' (ready to run)
    taskPtrTaskCpy->last_state = RTOS_state_idle;
    taskPtrTaskCpy->state = RTOS_state_idle;
}

// service handler: delay current task
RTOS_KERNEL_NOINLINE(RTOS_sysSrvHnd_skip, const u16 u16Skip_msCpy)
{
    if (u16Skip_msCpy)
    {
        // +1 because this a "length" model rather than a "distance" model.
        // ex: if we're at time slice 't', and we want to skip by 1,
        //     in "length" model: skip by 1 means skip 't+1' and start at 't+2'
        //     in "distance" model: skip by 1 means skipping the entire distance between
        //                          't' and 't+1', and start at 't+1'
        // this is why +1 is added to the below expression, to make it a "length" model.
        // notice how the "length" model is ahead of the "distance" model by '1'

        current_task->ticks = RTOS_u16Ticks_ms + u16Skip_msCpy + 1;
        current_task->state = RTOS_state_delayed;

        // immediate context switch
        RTOS_getNextTask();
    }
}

// service handler: delete certain task
RTOS_KERNEL_NOINLINE(RTOS_sysSrvHnd_delete, const u8 u8IndexCpy)
{
    register RTOS_task_t* taskPtrTaskCpy = tasks + u8IndexCpy;

    // sanity check
    if (taskPtrTaskCpy >= (tasks + RTOS_USER_TASKS_LEN))
    {
        return;
    }

    // if this was a critical task, then leave the critical section
    if (isInsideCriticalSection && (taskPtrTaskCpy == critical_task))
    {
        isInsideCriticalSection = 0;
    }

    RTOS_implHandleDelete(taskPtrTaskCpy);

    // if the deleted task was the current one, then do an immediate context switch
    if (taskPtrTaskCpy == current_task)
    {
        RTOS_getNextTask();
    }
}

// service handler: acquire bin sem
RTOS_KERNEL_NOINLINE(RTOS_sysSrvHnd_acqBinSem, RTOS_sem_handle_t semHandleCpy)
{
    if ( !(*semHandleCpy) ) // if semaphore is already acquired (no counts left), then block task and switch context
    {
        // save semaphore's address for later, so we can check later if semaphore is released
        current_task->sem_addr = semHandleCpy;

        current_task->state = RTOS_state_blocked_bin_count_sem;

        // immediate context switch
        RTOS_getNextTask();
    }
    else // if semaphore is free (still has counts), then set it as acquired
    {
        *semHandleCpy = 0;
    }
}

// service handler: release bin sem
RTOS_KERNEL_NOINLINE(RTOS_sysSrvHnd_relBinSem, RTOS_sem_handle_t semHandleCpy)
{
    if ( !(*semHandleCpy) ) // if semaphore is acquired, then set it as free
    {
        *semHandleCpy = 1;
    }
    else // if semaphore is already free, then put task in error state and do a context switch
    {
        current_task->state = RTOS_state_ERROR_bin_sem_already_free;

#if (RTOS_USE_ERROR_CALLBACK != 0)
        RTOS_handleErrorCB(RTOS_state_ERROR_bin_sem_already_free);
#endif // RTOS_USE_ERROR_CALLBACK

        // immediate context switch
        RTOS_getNextTask();
    }
}

// service handler: acquire counting sem
RTOS_KERNEL_NOINLINE(RTOS_sysSrvHnd_acqCountSem, RTOS_sem_handle_t semHandleCpy)
{
    if ( !(*semHandleCpy) ) // if semaphore is completely acquired (no counts left), then block task and switch context
    {
        // save semaphore's address for later, so we can check later if semaphore is released
        current_task->sem_addr = semHandleCpy;

        current_task->state = RTOS_state_blocked_bin_count_sem;

        // immediate context switch
        RTOS_getNextTask();
    }
    else // if semaphore still has counts (still free), then decrement its count (acquire a count)
    {
        *semHandleCpy -= 1;
    }
}

// service handler: release counting sem
RTOS_KERNEL_NOINLINE(RTOS_sysSrvHnd_relCountSem, RTOS_sem_handle_t semHandleCpy)
{
    *semHandleCpy += 1; // increase the semaphore's count (release a count)
}

// service handler: release counting sem (safe)
RTOS_KERNEL_NOINLINE(RTOS_sysSrvHnd_relCountSemSafe, RTOS_sem_handle_t semHandleCpy, u16 maxCount)
{
    // if (current count = maxCount), then, task is error-ed
    if (*semHandleCpy >= maxCount)
    {
        current_task->state = RTOS_state_ERROR_count_sem_max_free;

#if (RTOS_USE_ERROR_CALLBACK != 0)
        RTOS_handleErrorCB(RTOS_state_ERROR_count_sem_max_free);
#endif // RTOS_USE_ERROR_CALLBACK

        // immediate context switch
        RTOS_getNextTask();
    }
    else
    {
        *semHandleCpy += 1; // increase the semaphore's count (release a count)
    }
}

// service handler: enter critical section
RTOS_KERNEL_NOINLINE(RTOS_sysSrvHnd_enterCriticalSection, void)
{
    if (!isInsideCriticalSection) // if (NOT inside a critical section), then enter critical section
    {
        critical_task = current_task;
        isInsideCriticalSection = 1;
        current_task->state = RTOS_state_critical_running;

    }
    else                          // if (already inside a critical section), then put task in error state and switch context
    {
        current_task->state = RTOS_state_ERROR_multi_critical_sections;

#if (RTOS_USE_ERROR_CALLBACK != 0)
        RTOS_handleErrorCB(RTOS_state_ERROR_multi_critical_sections);
#endif // RTOS_USE_ERROR_CALLBACK

        // immediate context switch
        RTOS_getNextTask();
    }
}

// service handler: leave critical section
RTOS_KERNEL_NOINLINE(RTOS_sysSrvHnd_leaveCriticalSection, void)
{
    if (isInsideCriticalSection) // if (already inside a critical section), then leave it
    {
        isInsideCriticalSection = 0;
        current_task->state = RTOS_state_resumed;
    }
    else                         // if (NOT inside a critical section), then put task in error state and switch context
    {
        current_task->state = RTOS_state_ERROR_unentered_critical_section;

#if (RTOS_USE_ERROR_CALLBACK != 0)
        RTOS_handleErrorCB(RTOS_state_ERROR_unentered_critical_section);
#endif // RTOS_USE_ERROR_CALLBACK

        // immediate context switch
        RTOS_getNextTask();
    }
}

// service handler: pause certain task
RTOS_KERNEL_NOINLINE(RTOS_sysSrvHnd_pause, const u8 u8IndexCpy)
{
    register RTOS_task_t* taskPtrTaskCpy = tasks + u8IndexCpy;

    // sanity check
    if (taskPtrTaskCpy >= (tasks + RTOS_USER_TASKS_LEN))
    {
        return;
    }

    RTOS_implHandlePause(taskPtrTaskCpy);

    // if the paused task was the current one, then do an immediate context switch
    if (taskPtrTaskCpy == current_task)
    {
        RTOS_getNextTask();
    }
}

// service handler: reset certain task
RTOS_KERNEL_NOINLINE(RTOS_sysSrvHnd_reset, const u8 u8IndexCpy)
{
    register RTOS_task_t* taskPtrTaskCpy = tasks + u8IndexCpy;

    // sanity check
    if (taskPtrTaskCpy >= (tasks + RTOS_USER_TASKS_LEN))
    {
        return;
    }

    RTOS_implHandleReset(taskPtrTaskCpy);

    // if this was a critical task, then leave the critical section
    if (isInsideCriticalSection && (taskPtrTaskCpy == critical_task))
    {
        isInsideCriticalSection = 0;
    }

    // if the paused task was the current one, then do an immediate context switch
    if (taskPtrTaskCpy == current_task)
    {
        RTOS_getNextTask();
    }
}

// service handler: illegal request
RTOS_KERNEL_NOINLINE(RTOS_sysSrvHnd_illegal, void)
{
    // if this was a critical task, then leave the critical section
    if (isInsideCriticalSection && (current_task == critical_task))
    {
        isInsideCriticalSection = 0;
    }

    current_task->state = RTOS_state_ERROR_illegal_request;

#if (RTOS_USE_ERROR_CALLBACK != 0)
        RTOS_handleErrorCB(RTOS_state_ERROR_illegal_request);
#endif // RTOS_USE_ERROR_CALLBACK

    // immediate context switch
    RTOS_getNextTask();
}
// ************ //


// *** RTOS ISRs *** //
#if (RTOS_COOKIE_ == 1)
// triggered by timer
__attribute__((externally_visible, noinline, used)) void SysTick_Handler(void)
{
    RTOS_DISABLE_GINT();

    RTOS_INC_TICKS_COUNTER();

    // set PendSV exception to pending state, this does the actual context switch
    RTOS_PEND_CONTEXT_SWITCH();

    RTOS_ENABLE_GINT();
}

// system service request handler/receiver, called by tasks
__attribute__((externally_visible, naked, noinline, used)) void SVC_Handler(void)
{
    RTOS_DISABLE_GINT();

    RTOS_SAVE_CURRENT_CONTEXT();

    // save EXEC_RETURN value in LR because it gets corrupted by these calls
    RTOS_PUSH_REG(lr);

    // prepare args for dispatcher (from task's stack) and call it
    asm volatile(
                 "mrs r1, psp \t\n" /* r1 = PSP // addr of r0 from task's stack, used as args in dispatcher */
                 "ldr r0, [r1, #24] \t\n" /* r0 = PC // r0 = M:[(u32*)PSP + 6] = M:[PSP + 6*4] */
                 "ldrb r0,[r0, #-2] \t\n" /* r0 = M:[PC - 2] // r0 = request ID */
                 "bl RTOS_sysSrvDispatcher \t\n" /* call services dispatcher(r0, r1) */
                );

    // restore EXEC_RETURN value to LR
    RTOS_POP_REG(lr);

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_ENABLE_GINT();

    RTOS_RETI;
}

// triggered (as a very late interrupt) by SysTick or when a system service is requested
__attribute__((externally_visible, naked, noinline, used)) void PendSV_Handler(void)
{
    RTOS_DISABLE_GINT();

    RTOS_SAVE_CURRENT_CONTEXT();

    // save EXEC_RETURN value in LR because it gets corrupted by these calls
    RTOS_PUSH_REG(lr);

#if (RTOS_OVERFLOW_CHECK != 0)
    RTOS_handleOverflow();
#endif // RTOS_OVERFLOW_CHECK

    RTOS_getNextTask();

    // restore EXEC_RETURN value to LR
    RTOS_POP_REG(lr);

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_ENABLE_GINT();

    RTOS_RETI;
}
#endif // RTOS_COOKIE_
// ************ //
// --------- //


RTOS_API(void, RTOS_init, u32 AHB_clk)
{
    // disable global INT flag
    RTOS_DISABLE_GINT();

    // setup timer (initially stopped)
    PSTK_enable(0);
    PSTK_enableINT(0);
    PSTK_resetAllValues();
    PSTK_setClkSource(STK_clk_AHB);
    PSTK_setLoadValue(AHB_clk / 1000 - 1);

    // reset all tasks
    for (register RTOS_task_t* t = &(tasks[0]); t < (tasks + RTOS_USER_TASKS_LEN); t += 1)
    {
        t->stack_ptr = 0;
        t->state = RTOS_state_uninitialized;
        t->ticks = 0;
        t->sem_addr = 0;
        t->last_state = RTOS_state_uninitialized;
        t->stack_top = 0;
        t->stack_user_size = 0;
        t->function_top = 0;
    }

    // clear internal stacks (implicitly GP registers too)
#if (RTOS_USE_EXTERNAL_STACK == 0)
    for (register RTOS_stack_t i = 0; i < RTOS_USER_TASKS_LEN; i++)
    {
        for (register RTOS_SP_t s = stack[i]; s < (stack[i] + RTOS_INTERNAL_STACK_LEN); s += 1)
        {
            *s = 0;
        }
    }
#endif // RTOS_USE_EXTERNAL_STACK

    // init background task
    background_task->stack_ptr = 0;
    background_task->state = RTOS_state_idle;
    background_task->ticks = 0;
    background_task->sem_addr = 0;
    background_task->last_state = RTOS_state_idle;
    background_task->stack_top = (RTOS_SP_t)stack_bg_task;
    background_task->stack_user_size = RTOS_BACKGROUND_STACK_LEN;
    background_task->function_top = 0;

    // config priorities of INTs
    // priority of SysTick = 0 (highest)
    PSCB_setSysTickPriority(0);
    // priority of SVCall = 14 (pre-last)
    PSCB_setSVCallPriority(14);
    // priority of PendSV = 15 (last)
    PSCB_setPendSVpriority(15);
}

RTOS_API(void, RTOS_start, void)
{
    RTOS_POINT_PSP_TO_BG_STACK();

    // enable global INT flag
    RTOS_ENABLE_GINT();

    // enable SysTick timer
    PSTK_enableINT(1);
    PSTK_enable(1);

    RTOS_SWITCH_MODE_TO_UNPRIVILEGED();

    // this is the background task
    while (1)
    {

    }
}

#if (RTOS_USE_EXTERNAL_STACK == 0)
RTOS_API(void, RTOS_registerTask, const RTOS_PC_t funcPtrFunctionCpy, const u16 u16Skip_msCpy, const u8 u8isPaused, const u8 u8IndexCpy)
#else // if using external stack
RTOS_API(void, RTOS_registerTask, const RTOS_PC_t funcPtrFunctionCpy, const u16 u16Skip_msCpy, const u8 u8isPaused, const RTOS_SP_t stackPtrStkCpy, const u16 u16StkLenCpy, const u8 u8IndexCpy)
#endif // RTOS_USE_EXTERNAL_STACK
{
    register RTOS_task_t* taskPtrTaskCpy = tasks + u8IndexCpy;

    // sanity check
    if (taskPtrTaskCpy >= (tasks + RTOS_USER_TASKS_LEN))
    {
        return;
    }

#if (RTOS_USE_EXTERNAL_STACK == 0)
    taskPtrTaskCpy->stack_user_size = RTOS_USER_STACK_SIZE;

    taskPtrTaskCpy->stack_top = stack[(u8)(taskPtrTaskCpy - tasks)]; // point at top of stack
#else // if using external stack
    taskPtrTaskCpy->stack_user_size = u16StkLenCpy;

    taskPtrTaskCpy->stack_top = stackPtrStkCpy; // point at top of stack
#endif // RTOS_USE_EXTERNAL_STACK

    // save function address
    taskPtrTaskCpy->function_top = funcPtrFunctionCpy;

    RTOS_implHandleReset(taskPtrTaskCpy);

    taskPtrTaskCpy->ticks = u16Skip_msCpy;

    if (u16Skip_msCpy) // if initially delayed
    {
        taskPtrTaskCpy->last_state = RTOS_state_delayed;

        if (u8isPaused) // if also initially paused
        {
            taskPtrTaskCpy->state = RTOS_state_suspended;
        }
        else
        {
            taskPtrTaskCpy->state = RTOS_state_delayed;
        }
    }
    else               // if initially NOT delayed
    {
        taskPtrTaskCpy->last_state = RTOS_state_idle;

        if (u8isPaused) // if also initially paused
        {
            taskPtrTaskCpy->state = RTOS_state_suspended;
        }
        else
        {
            taskPtrTaskCpy->state = RTOS_state_idle;
        }
    }
}

RTOS_API(void, RTOS_deregisterTask, const u8 u8IndexCpy)
{
    (void)u8IndexCpy;
    RTOS_SVC_DELETE_CERTAIN_TASK();
}

// skip the next 'N' slices, then timeout and start at slice 'N+1'
RTOS_API(void, RTOS_skip, u16 u16Skip_msCpy)
{
    (void)u16Skip_msCpy;
    RTOS_SVC_DELAY_CURRENT_TASK();
}

RTOS_API(void, RTOS_acquireBinSem, RTOS_sem_handle_t semHandleCpy)
{
    (void)semHandleCpy;
    RTOS_SVC_ACQ_BIN_SEM();
}

RTOS_API(void, RTOS_releaseBinSem, RTOS_sem_handle_t semHandleCpy)
{
    (void)semHandleCpy;
    RTOS_SVC_REL_BIN_SEM();
}

RTOS_API(void, RTOS_acquireCountingSem, RTOS_sem_handle_t semHandleCpy)
{
    (void)semHandleCpy;
    RTOS_SVC_ACQ_COUNT_SEM();
}

RTOS_API(void, RTOS_releaseCountingSem, RTOS_sem_handle_t semHandleCpy)
{
    (void)semHandleCpy;
    RTOS_SVC_REL_COUNT_SEM();
}

RTOS_API(void, RTOS_releaseCountingSemSafe, RTOS_sem_handle_t semHandleCpy, u16 maxCount)
{
    (void)semHandleCpy;
    (void)maxCount;
    RTOS_SVC_REL_SAFE_COUNT_SEM();
}

RTOS_API(void, RTOS_enterCriticalSection, void)
{
    RTOS_SVC_ENTER_CRITICAL_SECTION();
}

RTOS_API(void, RTOS_leaveCriticalSection, void)
{
    RTOS_SVC_LEAVE_CRITICAL_SECTION();
}

RTOS_API(void, RTOS_pauseCurrentTask, void)
{
    RTOS_SVC_PAUSE_CURRENT_TASK();
}

RTOS_API(void, RTOS_pauseTask, const u8 u8IndexCpy)
{
    (void)u8IndexCpy;
    RTOS_SVC_PAUSE_CERTAIN_TASK();
}

RTOS_API(void, RTOS_resumeTask, const u8 u8IndexCpy)
{
    register RTOS_task_t* taskPtrTaskCpy = tasks + u8IndexCpy;

    // sanity check,
    // also resuming current task is NOT allowed because it's useless!
    if ((taskPtrTaskCpy >= (tasks + RTOS_USER_TASKS_LEN)) || (taskPtrTaskCpy == current_task))
    {
        return;
    }

    // NOTE: resuming an already resumed task shouldn't be allowed
    //       because the second resume may put incorrect data in 'state'
    // ex: at start     -> last_state = idle, state = idle
    //     first cycle  -> state = resumed
    //     first resume -> state = last_state = idle (wrong)

    if (taskPtrTaskCpy->state != RTOS_state_suspended)
    {
        return;
    }

    // get previous/original task state
    // TODO: is this true ? this is a "blind" change of state, maybe more logic should be added
    taskPtrTaskCpy->state = taskPtrTaskCpy->last_state;
}

// this is also used when normal 'return' is used, will switch context after resetting task attributes
RTOS_API(void, RTOS_resetCurrentTask, void)
{
    RTOS_SVC_RESET_CURRENT_TASK();
}

RTOS_API(void, RTOS_resetTask, const u8 u8IndexCpy)
{
    (void)u8IndexCpy;
    RTOS_SVC_RESET_CERTAIN_TASK();
}

RTOS_API(void, RTOS_giveUp, void)
{
    RTOS_SVC_CONTEXT_SWITCH();
}

RTOS_API(u16, RTOS_getTicksCount, void)
{
    return RTOS_u16Ticks_ms;
}

RTOS_API(RTOS_task_state_t, RTOS_getCurrentTaskState, void)
{
    return current_task->state;
}

RTOS_API(RTOS_task_state_t, RTOS_getTaskState, const u8 u8IndexCpy)
{
    // sanity check
    if (u8IndexCpy >= RTOS_USER_TASKS_LEN)
    {
        return RTOS_state_uninitialized;
    }

    return tasks[u8IndexCpy].state;
}

#if (RTOS_USE_ERROR_CALLBACK != 0)
RTOS_API(void, RTOS_registerErrorCB, const RTOS_error_CB_t funcPtrErrorCBCpy)
{
    RTOS_CB_error = funcPtrErrorCBCpy;
}

RTOS_API(void, RTOS_deregisterErrorCB, void)
{
    RTOS_CB_error = 0;
}

RTOS_API(void, RTOS_registerCriticalErrorCB, const RTOS_error_CB_t funcPtrErrorCBCpy)
{
    RTOS_CB_critical_error = funcPtrErrorCBCpy;
}

RTOS_API(void, RTOS_deregisterCriticalErrorCB, void)
{
    RTOS_CB_critical_error = 0;
}
#endif // RTOS_USE_ERROR_CALLBACK


#endif // RTOS_COOKIE_

